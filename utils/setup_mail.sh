#!/bin/bash

{ \
  echo "root=postmaster"; \
  echo "mailhub=$MAILHUB"; \
  echo "FromLineOverride=YES"; \
} > /etc/ssmtp/ssmtp.conf

if ([ ! -z "$MAILHUB" ]) then
  phpenmod ssmtp
else
  phpdismod ssmtp
fi
