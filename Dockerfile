ARG PHP_VERSION
ARG IMAGE_VARIANT

FROM thecodingmachine/php:${PHP_VERSION}-v4-${IMAGE_VARIANT}

USER root

RUN set -eux; \
    apt-get update; \
    apt-get install -y --no-install-recommends bash-completion; \
    rm -rf /var/lib/apt/lists/*

COPY utils/.bashrc /root/.bashrc
COPY --chown=docker:docker utils/.bashrc /home/docker/.bashrc

COPY utils/setup_mail.sh /usr/local/bin/setup_mail.sh

RUN set -eux; \
    apt-get update; \
    apt-get install -y --no-install-recommends ssmtp; \
    rm -rf /var/lib/apt/lists/*; \
    sed -i '/setup_extensions\.php/ a /usr/local/bin/setup_mail.sh' /usr/local/bin/docker-entrypoint-as-root.sh; \
    echo "sendmail_path = /usr/sbin/ssmtp -t" > /etc/php/${PHP_VERSION}/mods-available/ssmtp.ini;

ARG NODE_VERSION
RUN if [ -n "$NODE_VERSION" ]; then \
    apt-get update; \
    apt-get install -y --no-install-recommends gnupg; \
    curl -sL https://deb.nodesource.com/setup_${NODE_VERSION}.x | bash - ; \
    apt-get update; \
    apt-get install -y --no-install-recommends nodejs; \
    curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | apt-key add - ; \
    echo "deb https://dl.yarnpkg.com/debian/ stable main" | tee /etc/apt/sources.list.d/yarn.list; \
    apt-get update; \
    apt-get install -y --no-install-recommends yarn; \
    rm -rf /var/lib/apt/lists/*; \
    fi;

USER docker

RUN echo 'source ~/.bashrc' > ~/.bash_profile; \
    echo 'eval "$(symfony-autocomplete --shell=bash)"' >> ~/.bashrc
